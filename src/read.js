import React from 'react'

class Read extends React.Component{
	render(){

		let readbooks
		readbooks = this.props.books


	 return(
	 	<div className="bookshelf">
                  <h2 className="bookshelf-title">Read</h2>
                  <div className="bookshelf-books">
                    <ol className="books-grid">
                    {

                    readbooks.filter((books) => books.shelf === 'read').map((data,index)=>(<li key={index}>
                      
                        <div className="book">
                          <div className="book-top">
                            <div className="book-cover" style={{ width: 128, height: 192, backgroundImage: `url(${data.imageLinks.thumbnail})` }}></div>
                            <div className="book-shelf-changer">
                              <select>
                                <option value="move" disabled>Move to...</option>
                                <option value="currentlyReading">Currently Reading</option>
                                <option value="wantToRead">Want to Read</option>
                                <option value="read">Read</option>
                                <option value="none">None</option>
                              </select>
                            </div>
                          </div>
                          <div className="book-title">{data.title}</div>
                          <div className="book-authors">{data.authors[0]}</div>
                        </div>
                      </li>
                      ))
                      
                    }
                      
                    </ol>
                  </div>
                </div>
	 )
	}
}
export default Read;