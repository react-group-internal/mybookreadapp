import React from 'react'

class Currentread extends React.Component{

	render(){
		//console.log('props',this.props.books);
		let currentbooks
		currentbooks = this.props.books
		//console.log(currentbooks)
		return (
			<div className="bookshelf">
                  <h2 className="bookshelf-title">Currently Reading</h2>
                  <div className="bookshelf-books">
                    <ol className="books-grid">
                    {
                    	currentbooks.filter((books) => books.shelf === 'currentlyReading').map((data,index)=>(<li key={index}>
                        <div className="book">
                          <div className="book-top">
                          
                            <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: `url(${data.imageLinks.thumbnail})` }}></div>
                            <div className="book-shelf-changer">
                              <select>
                                <option value="move" disabled>Move to...</option>
                                <option value="currentlyReading">Currently Reading</option>
                                <option value="wantToRead">Want to Read</option>
                                <option value="read">Read</option>
                                <option value="none">None</option>
                              </select>
                            </div>
                          </div>
                          <div className="book-title">{data.title}</div>
                          <div className="book-authors">{data.authors[0]}</div>
                        </div>
                      </li>

                    		))
                    }
                      
                      
                    </ol>
                  </div>
                </div>
			)

	}
}
export default Currentread